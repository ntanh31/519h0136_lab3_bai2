import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:lab3_bai2/module/photo.dart';

class StreamPhoto{
  Stream<Photo> getPhotos() async* {
    final response = await http.get(Uri.parse('https://jsonplaceholder.typicode.com/photos'));

    var jsonRaw = response.body;
    var jsonObject = json.decode(jsonRaw);

    List<Photo> photos = [];

    for(var i in jsonObject){
      Photo photo = Photo.fromJson(i);
      photos.add(photo);
    }

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % photos.length;
      return photos[index];
    });
  }
}