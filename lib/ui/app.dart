import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab3_bai2/stream/stream_photo.dart';
import '../module/photo.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  StreamPhoto streamPhoto = StreamPhoto();
  List<Photo> listPhoto = [];
  Photo? photo;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    streamPhoto.getPhotos().listen((event) {
      setState(() {
        photo = event;
        listPhoto.add(photo!);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Stream New Words",
      home: Scaffold(
          appBar: AppBar(title: Text('App Bar'),),
          body:ListView.builder(
              itemCount: listPhoto.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Image.network(listPhoto[index].url),
                    ),
                    Center(
                      child: Center(child: Text(listPhoto[index].id.toString() + "  " + listPhoto[index].title, style: TextStyle(fontSize: 20),)),
                    ),
                    SizedBox(height: 20,),
                  ],
                );
              }
          )
      ),
    );
  }

}
